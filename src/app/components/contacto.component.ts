import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ContactoService } from '../services/contacto.service';
import { Contact } from '../models/contact';
import { PhoneNumber } from '../models/phone-number';
import { Country } from '../models/country';
import { Contacto } from '../models/contacto';
import { LocationContact } from '../models/location-contact';

@Component({
    selector: 'app-contacto',
    templateUrl: '../views/contacto.html',
    providers: [ContactoService]
})
export class ContactoComponent {
    public titulo: string;
    public contacto: Contacto;
    public contact: Contact;
    public phone: PhoneNumber;
    public country: Country;
    public locationContact: LocationContact;
    public pais: Array<Object>;
    public bandera: any;
    constructor(
        private _contactoService: ContactoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.titulo = 'Crear un nuevo contacto';
        this.country = new Country(0, '', true);
        this.phone = new PhoneNumber(this.country, 0);
        this.contact = new Contact('', '', '', '', this.phone, '');
        // La locación la introduciré en duro por que no se de donde se podrían sacar esos datos.
        this.locationContact = new LocationContact('124.2', '214.4');
        /*
        El registrado por lo introduciré en duro por que no hice un login o algo para que
        almacene el usuario que subió el contacto.
        Y el tipo siempre tiene que ser 1 según las especificaciones
        */
        this.contacto = new Contacto(this.contact, this.locationContact, 'Giovanni', 1);
    }

    ngOnInit() {
        this._contactoService.getPaises().subscribe(
            response => {
                this.pais = response;
            },
            error => {
                console.log('Error al traer paises: ', error);
            }
        );
    }

    onSubmit() {
        console.log(this.contacto);
        this.bandera = true;
        this._contactoService.postContact(this.contacto).subscribe(
            response => {
                if (response.code === 200) {
                    this._router.navigate(['/home']);
                } else {
                    console.log(response);
                }
            },
            error => {
                console.log(error);
            }
        );
    }
}
