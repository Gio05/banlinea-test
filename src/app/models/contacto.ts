import { Contact } from './contact';
import { LocationContact } from './location-contact';

export class Contacto {
    constructor(
        public contact: Contact,
        public location: LocationContact,
        public regiteredBy: string,
        public type: number,
    ) { }
}
