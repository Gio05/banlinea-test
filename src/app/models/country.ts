export class Country {
    constructor(
        public code: number,
        public name: string,
        public enabled: boolean
    ) { }
}
