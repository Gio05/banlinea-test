import { Country } from './country';

export class PhoneNumber {
    constructor(
        public country: Country,
        public number: number,
    ) { }
}
