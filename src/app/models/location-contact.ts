export class LocationContact {
    constructor(
        public latitude: string,
        public longitude: string,
    ) { }
}
