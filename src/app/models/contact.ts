import { PhoneNumber } from './phone-number';

export class Contact {
    constructor(
        public company: string,
        public emailAddress: string,
        public lastName: string,
        public name: string,
        // Cambiar el tipo a PhoneNumber
        public phoneNumber: PhoneNumber,
        public photo: string,
    ) { }
}
