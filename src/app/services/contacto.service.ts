import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Contacto } from '../models/contacto';
import { GLOBAL } from './global';

@Injectable()
export class ContactoService {
    public url: string;

    constructor(
        public _http: Http,
    ) {
        this.url = GLOBAL.url;
    }

    getPaises() {
        return this._http.get(this.url + 'countries').map(res => res.json());
    }

    postContact(contact: Contacto) {
        const json = JSON.stringify(contact);
        const params = 'json=' + json;
        const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

        return this._http.post(this.url + 'ContactRegister/', params, {headers: headers}).map(res => res.json());
    }
}
